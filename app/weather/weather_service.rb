class WeatherService
  def initialize(weather_data_service)
    @weather_data_service = weather_data_service
  end

  def is_it_hot(lat, lon)
    current_weather = @weather_data_service.get_weather(lat, lon)
    if current_weather['main']['temp'] > 30
      'hace calor, ponete los cortos'
    elsif current_weather['main']['temp'] < 10
      'ta fresco, ponete los largos'
    else
      'ta bien, dale tranqui'
    end
  end
end
