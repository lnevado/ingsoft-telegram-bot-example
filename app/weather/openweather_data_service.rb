class OpenWeatherDataService
  def initialize
    @connection = Faraday.new(url: 'http://api.openweathermap.org')
  end

  def get_weather(lat, lon)
    response = @connection.get('/data/2.5/weather') do |req|
      req.params['lat'] = lat
      req.params['lon'] = lon
      req.params['units'] = 'metric'
      req.params['appid'] = ENV['OPEN_WEATHER_API_KEY']
    end
    JSON.parse(response.body)
  end
end
