require 'spec_helper'

require "#{File.dirname(__FILE__)}/../../app/weather/weather_service"

describe 'WeatherService' do
  let(:weather_data_service) { instance_double('WeatherDataService') }
  let(:weather_service) { WeatherService.new(weather_data_service) }

  it 'should return "hace calor, ponete los cortos" when temp is greater than 30' do
    allow(weather_data_service).to receive(:get_weather).with(0, 0).and_return({ 'main' => { 'temp' => 31 } })
    expect(weather_service.is_it_hot(0, 0)).to eq('hace calor, ponete los cortos')
  end

  it 'should return "ta fresco, ponete los largos" when temp is less than 10' do
    allow(weather_data_service).to receive(:get_weather).with(0, 0).and_return({ 'main' => { 'temp' => 9 } })
    expect(weather_service.is_it_hot(0, 0)).to eq('ta fresco, ponete los largos')
  end

  it 'should return "ta bien, dale tranqui" when temp is between 10 and 30' do
    allow(weather_data_service).to receive(:get_weather).with(0, 0).and_return({ 'main' => { 'temp' => 20 } })
    expect(weather_service.is_it_hot(0, 0)).to eq('ta bien, dale tranqui')
  end
end
